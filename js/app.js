const MEMSIZE = 1024 * 64;
const PC_START = 0x1000

const createMemory = function () {
	const data = [];
	const size = MEMSIZE;
	return {
		size,
		
		init: function() {
			for (let index = 0; index < MEMSIZE; index++) {
				data[index] = 0
			}
		},

		readByte: (addr) => {
			return data[addr];
		},

		readWord: (addr) => {
			return data[addr] + data[addr] * 256;
		},

		writeByte: (addr, b) => {
			data[addr] = b & 0xFF;
		},

		writeWord: (addr, w) => {
			data[addr] = w & 0x00FF;
			data[addr+1] = w & 0xFF;
		}
	}
}



const createCpu = (memory, userConfig = {}) => {
	// REGISTERS
	let A,X,Y,P,SP,PC;
	let N,V,B,D,I,Z,C;
	let cycle;
	let halted;
	
	const reset = () => {
		cycle = 0;
		A = X = Y = P = SP = 0;
		N = V = B = D = I = Z = C = 0;
		PC = PC_START;
		halted = false;
	};

	const forwardPC = (steps) => {
		PC += steps;
		if (PC >= memory.size) {
			PC = PC - memory.size
		};
	};

	const readByte = () => {
		v = memory.readByte(PC);
		forwardPC(1);
		return v;
	};

	const readWord = () => {
		v = memory.readWord(PC);
		forwardPC(2);
		return v;
	};

	const updateP = () => {
		P = 0b00100000 | N << 7 | V << 6 | B << 4 | D << 3 | I << 2 | Z << 1 | C ;
	};

	const setFlag = (flag) => {
		switch (flag) {
			case 'N': N = 1; break;
			case 'V': V = 1; break;
			case 'B': B = 1; break;
			case 'D': D = 1; break;
			case 'I': I = 1; break;
			case 'Z': Z = 1; break;
			case 'C': C = 1; break;
		}
		updateP();
	}

	const clearFlag = (flag) => {
		switch (flag) {
			case 'N': N = 0; break;
			case 'V': V = 0; break;
			case 'B': B = 0; break;
			case 'D': D = 0; break;
			case 'I': I = 0; break;
			case 'Z': Z = 0; break;
			case 'C': C = 0; break;
		}
		updateP();
	}

	const updateFlag = flag => {
		switch (flag) {
			case 'N':
					N = (A & bx1000000) >> 7;
				break;
			case 'Z':
					Z = (A=0)?1:0;
				break;
		
			default:
				break;
		}
		updateP();
	}


	const updateFlags = flags => {
		for (flag in flags) {
			updateFlag(flag);
		}
	}

	
	const opcodes = [

//////////////////////////////////////////////////////////////////////
		
		{ 
			mnemonic: 'LDA',
			addrMode: 'IMM',
			opcode: 0xA9, 
			len:2, 
			exec: () => {
				let time = 2;
				A = readByte();
				updateFlags(['N','Z']);
				return time;
			}	
		},
		{ 
			mnemonic: 'LDA',
			addrMode: 'ZP',
			opcode: 0xA5, 
			len:2, 
			exec: () => {
				let time = 3;
				const zpaddr = readByte();
				A = memory.readByte(zpaddr);
				updateFlags(['N','Z']);
				return time;
			}	
		},
		{ 
			mnemonic: 'LDA',
			addrMode: 'ZPX',
			opcode: 0xB5, 
			len:2, 
			exec: () => {
				let time = 4;
				const zpaddr = (readByte() + X) & 0xFF;
				A = memory.readByte(zpaddr);
				updateFlags(['N','Z']);
				return time;
			}	
		},
		{ 
			mnemonic: 'LDA',
			addrMode: 'ABS',
			opcode: 0xAD, 
			len:3, 
			exec: () => {
				let time = 4;
				const addr = readWord();
				A = memory.readByte(addr);
				updateFlags(['N','Z']);
				return time;
			}	
		},

		{ 
			mnemonic: 'LDA',
			addrMode: 'ABX',
			opcode: 0xBD, 
			len:3, 
			exec: () => {
				let time = 4;
				const base = readWord()
				const addr = (base + X) & 0xFFFF;
				if ((addr & 0xFF00) != (base & 0xFF00)) {time++};
				A = memory.readByte(addr);
				updateFlags(['N','Z']);
				return time;
			}	
		},

		{ 
			mnemonic: 'LDA',
			addrMode: 'ABY',
			opcode: 0xB9, 
			len:3, 
			exec: () => {
				let time = 4;
				const base = readWord()
				const addr = (base + X) & 0xFFFF;
				if ((addr & 0xFF00) != (base & 0xFF00)) {time++};
				A = memory.readByte(addr);
				updateFlags(['N','Z']);
				return time;
			}	
		},

		{ 
			mnemonic: 'LDA',
			addrMode: 'IDX',
			opcode: 0xA1, 
			len:2, 
			exec: () => {
				let time = 6;
				const zp = readByte()
				const addr = (zp + X) & 0xFF;
				A = memory.readByte(addr);
				updateFlags(['N','Z']);
				return time;
			}	
		},

		{ 
			mnemonic: 'LDA',
			addrMode: 'IDY',
			opcode: 0xB1, 
			len:2, 
			exec: () => {
				let time = 5;
				const zp = readByte()
				const base = memory.readWord(zp);
				const addr = (base + Y) & 0xFFFF;
				if ((addr & 0xFF00) != (base & 0xFF00)) {time++};
				A = memory.readByte(addr);
				updateFlags(['N','Z']);
				return time;
			}	
		},


////////////////////////////////////////////////////////////////////////

		{ 
			mnemonic: 'STA',
			addrMode: 'ZP',
			opcode: 0x85, 
			len:2, 
			exec: () => {
				let time = 3;
				const zpaddr = readByte();
				memory.writeByte(zpaddr,A);
				return time;
			}	
		},

		{ 
			mnemonic: 'STA',
			addrMode: 'ZPX',
			opcode: 0x95, 
			len:2, 
			exec: () => {
				let time = 4;
				const zpaddr = (readByte() + X) & 0xFF;
				memory.writeByte(zpaddr,A);
				return time;
			}	
		},


		{ 
			mnemonic: 'STA',
			addrMode: 'ABS',
			opcode: 0x8D, 
			len:3, 
			exec: () => {
				let time = 4;
				const addr = readWord();
				memory.writeByte(addr,A);
				return time;
			}	
		},

		{ 
			mnemonic: 'STA',
			addrMode: 'ABX',
			opcode: 0x9D, 
			len:3, 
			exec: () => {
				let time = 5;
				const base = readWord()
				const addr = (base + X) & 0xFFFF;
				memory.writeByte(addr,A);
				return time;
			}	
		},

		{ 
			mnemonic: 'STA',
			addrMode: 'ABY',
			opcode: 0x99, 
			len:3, 
			exec: () => {
				let time = 5;
				const base = readWord()
				const addr = (base + X) & 0xFFFF;
				memory.writeByte(addr,A);
				return time;
			}	
		},

		{ 
			mnemonic: 'STA',
			addrMode: 'IDX',
			opcode: 0x81, 
			len:2, 
			exec: () => {
				let time = 6;
				const zp = readByte()
				const addr = (zp + X) & 0xFF;
				memory.writeByte(addr,A);
				return time;
			}	
		},

		{ 
			mnemonic: 'STA',
			addrMode: 'IDY',
			opcode: 0x91, 
			len:2, 
			exec: () => {
				let time = 6;
				const zp = readByte()
				const base = memory.readWord(zp);
				const addr = (base + Y) & 0xFFFF;
				memory.writeByte(addr,A);
				return time;
			}	
		}









	]
	


	const parseCommand = (opcode) => {
		let valid = false;
		var act = null;
		for (const cmd of opcodes) {
			if (cmd.opcode == opcode) {
				act = cmd;
				valid = true;
			}
		}
		return {
			valid, action: act
		}
	};

	const execute = () => {
		if (halted) {
			if (userConfig.onHalt) userConfig.onHalt();
			return null;
		}
		const command_opcode = readByte();
		const cmd = parseCommand(command_opcode);
		if (cmd.valid) {
			cycle += cmd.action.exec();
			if (userConfig.onCommand) userConfig.onCommand(cmd);
		} else {
			if (userConfig.onError) userConfig.onError(`Unknown opcode: ${command_opcode}`);
			halted = true;
			return null;
		}
		return cmd;
	};

	const status = () => {
		return {
			A,X,Y,P,SP,PC, 
			N,V,B,D,I,Z,C,
			halted,
			cycle
		}
	}

	memory.init();
	reset();

	return {
		reset,
		status,
		execute,
	}
};

const toMyHex = d => `$${d.toString(16).toUpperCase().padStart(2, '0')}`

const showCpuStatus = cpustat => {
	conWrite(`PC:${toMyHex(cpustat.PC)} A:${toMyHex(cpustat.A)} X:${toMyHex(cpustat.X)} Y:${toMyHex(cpustat.Y)} cycles:${cpustat.cycle}`);
}

const showMEM = (mem, addr, len, chunksize = 16) => {
	const dump = [];
	for (let index = 0; index < len; index++) {
		dump.push(mem.readByte(addr + index));
	}
	_.each(_.chunk(_.map(dump, toMyHex),chunksize),(chunk,idx)=>{
		conWrite(`${toMyHex(addr+idx*chunksize)}: ${chunk.join(',')}`);
	});
	
}

const conWrite = (txt) => {
	const div = $('<div/>').html(txt);
	$('#console').append(div);
}

const onHalt = () => {conWrite('CPU Halted!')};
const onError = (err) => {conWrite(`ERROR: ${err}`)};
const onCommand = (cmd) => {conWrite(`${cmd.action.mnemonic}:${cmd.action.addrMode}`)};

$(document).ready(function() {
	const app = gui();
	const mem = createMemory();
	const cpu = createCpu(mem, {onHalt, onCommand, onError});

	mem.writeByte(0x1000, 0xa9);
	mem.writeByte(0x1001, 0x66);
	mem.writeByte(0x1002, 0x8d);
	mem.writeWord(0x1003, 0x1010);

	app.addMenuItem('CPU status', () => {showCpuStatus(cpu.status())});
	app.addMenuItem('CPU Step', cpu.execute);
	app.addMenuItem('CPU Reset', cpu.reset);
	app.addMenuItem('Show MEM', () => {showMEM(mem, 0x1000, 32)});
	conWrite('app started');
	showCpuStatus(cpu.status());
	


});
